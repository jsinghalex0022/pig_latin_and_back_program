import java.util.Scanner;

public class Main{
    static String finalString = "";
    public static void main(String[] args){
        System.out.print("Select optionn:\n1) English to pig latin:\n2) Pig latin to english:\nYour option: ");
        Scanner scan = new Scanner(System.in);
        int option = scan.nextInt();
        if(option == 1){
            stripWords("The quick brown fox",1 );
        } else {
            stripWords("Hetay uickqay rownbay oxfay",2);
        }
        System.out.println finalString.trim());
    }

    //strips the word between two spaces
    public static void stripWords(String word, int option){
        int i = 0;
        while(i < word.length()){
            if(i == 0 || word.charAt(i) == ' '){
                for(int j = i+1; j < word.length(); j++){
                    if(word.charAt(j) == ' ' || j == word.length()-1){
                        String w = word.substring(i+1,j+1);

                        if(option == 1) {
                            if(i == 0) {
                                String x = "";
                                x = x + word.charAt(0);
                                x = x.toLowerCase();

                                String y = "";
                                y = y + word.charAt(1);
                                y = y.toUpperCase();
                                w = x + y + word.substring(i + 2, j);
                            }
                            convertToPigLatin(w.trim());
                        } else {
                            if(i == 0) {
                                w = w.replace("ay","");
                                String x = "";
                                x = x + word.charAt(0);
                                x = x.toLowerCase();

                                String y = "";
                                y = y + w.charAt(w.length()-2);
                                y = y.toUpperCase();
                                w = x + word.substring(i + 1, w.length()-1) + y;
                            }
                            convertToEnglish(w.trim());
                        }
                        i = j-1;
                        break;
                    }
                }
            }
            i++;
        }
    }

    public static void convertToPigLatin(String word){
        char[] wordArray = word.toCharArray();
        for(int i = 0; i < word.length(); i++){
            if(i != word.length()-1) {
                wordArray[i] = wordArray[i + 1];
            } else if(i == word.length()-1) {
                wordArray[word.length()-1] = word.charAt(0);
            }
        }
        String convertedString = "";
        for(int k = 0; k < wordArray.length; k++)
        {
            convertedString = convertedString + wordArray[k];
        }
        convertedString = convertedString + "ay";
        finalString = finalString + convertedString + " ";
    }

    public static void convertToEnglish(String word){
        word = word.replace("ay", "");
        char[] wordArray = word.toCharArray();
        for(int i = word.length()-1; i >= 0; i--){
            if(i != 0) {
                wordArray[i] = wordArray[i - 1];
            } else if(i == 0) {
                wordArray[0] = word.charAt(word.length()-1);
            }
        }
        String convertedString = "";
        for(int k = 0; k < wordArray.length; k++)
        {
            convertedString = convertedString + wordArray[k];
        }
        finalString = finalString + convertedString + " ";
    }

}